var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var cors = require("cors");
var sdk = require("emergepay-sdk");

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

//Ensure that you replace these with valid values before trying to issue a request
var oid = "1394243001";
var authToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0aWQiOjEyLCJmcW9pZCI6IjEtMTM5NDI0MzAwMSIsInRva2VuX3VzZSI6Im9ydCIsInJuZCI6MTI2MTI5MzY4Ni45MDc4MTc4LCJncm91cHMiOlsiT3JnQVBJVXNlcnMiXSwiaWF0IjoxNTIzOTk2OTQ5fQ.SpHIMHvvpg83oXLQPDoy8c5Z3k6fAXuSudZ9s2aaA9c";
var environmentUrl = "https://api.emergepay-sandbox.chargeitpro.com/virtualterminal/v1";
var emergepay = new sdk.emergepaySdk({ oid: oid, authToken: authToken, environmentUrl: environmentUrl });


//The client side can hit this endpoint by issuing a POST request to
//localhost:5555/start-transaction
//base_amount and external_tran_id are required in the fields array.
app.post("/start-transaction", function (req, res) {
    var amount = "0.01";
    var config = {
        transactionType: sdk.TransactionType.CreditSale,
        method: "modal",
        fields: [
            {
                id: "base_amount",
                value: amount
            },
            {
                id: "external_tran_id",
                value: emergepay.getExternalTransactionId()
            }
        ]
    };
  
    emergepay.startTransaction(config)
    .then(function (transactionToken) {
        res.send({
            transactionToken: transactionToken
        });
    })
    .catch(function (err) {
        res.send(err.message);
    });
});

console.log("listening on port 5555");
app.listen(5555);

module.exports = app;
